﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PinballBumper : PinballObject, ISequenceElement
{
    [SerializeField]
    private float _bumpForce = 40;

    [SerializeField]
    private Renderer _flashingRenderer;
    [SerializeField]
    private Color _flashColor;
    [SerializeField]
    private float _flashSpeed;

    public UnityEvent OnHit;

    private Color _baseColor;
    private Material _material;
    

    public PinballSequence Sequence { get; set; }

    [SerializeField]
    private string _elementMessage;
    public string ElementMessage {
        get { return _elementMessage; }
    }

    [SerializeField]
    private string _sequenceElementName;
    public string ElementName {
        get { return _sequenceElementName; }
    }


    private void Start()
    {
        _material = _flashingRenderer.material;
        _baseColor = _material.color;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Pinball"))
        {
            Vector3 bounceDir = Vector3.Reflect(other.relativeVelocity, other.contacts[0].normal);

            Rigidbody pbody = other.transform.GetComponent<Rigidbody>();

            if (pbody != null)
            {
                pbody.AddForceAtPosition(bounceDir.normalized * _bumpForce, other.contacts[0].point, ForceMode.Impulse);
                pbody.AddForceAtPosition(Vector3.down * _bumpForce * .5f, other.contacts[0].point, ForceMode.Impulse);
                Flash();
                OnHit.Invoke();
                if (Sequence != null)
                    Sequence.ProcessElement(this);
            }
        }
    }

    private void Flash() { StartCoroutine("FlashInner"); }

    private IEnumerator FlashInner()
    {

        _material.color = _flashColor;
        float startTime = Time.time;
        yield return new WaitForEndOfFrame();
        
        while (_material.color != _baseColor)
        {
            _material.color = Color.Lerp(_flashColor, _baseColor, (Time.time - startTime) * _flashSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    public override void Reset()
    {
        StopAllCoroutines();
        _material.color = _baseColor;
    }
}
