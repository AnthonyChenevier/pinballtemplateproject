﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Events;

public class PinballCatcher : PinballObject, ISequenceElement
{

    public bool KeepMode;


    public float EjectTime = 3;

    private float _pullTime = 0.1f;

    [SerializeField]
    private float _ejectForce;

    private Rigidbody _pinballBody;


    public UnityEvent OnCatch;
    public UnityEvent OnEject;


    public PinballSequence Sequence { get; set; }

    [SerializeField]
    private string _elementMessage;
    public string ElementMessage {
        get { return _elementMessage; }
    }


    [SerializeField]
    private string _sequenceElementName;
    public string ElementName {
        get { return _sequenceElementName; }
    }

    private Pinball _pinball;



    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pinball"))
        {
            SetBall(other.GetComponent<Pinball>());
            SetBody(other.attachedRigidbody);
            _pinballBody.isKinematic = true;
            StartCoroutine(MoveBall(_pinballBody.transform));

        }
    }

    private void SetBody(Rigidbody ballbody)
    {
        _pinballBody = ballbody;
    }

    private void SetBall(Pinball ball)
    {
        _pinball = ball;
    }

    private IEnumerator MoveBall(Transform ball)
    {
        Vector3 startPosition = ball.position;
        Vector3 endPosition = transform.position;
        float timeElapsed = 0;
        while (ball.position != transform.position)
        {
            timeElapsed += Time.deltaTime;
            ball.position = Vector3.Lerp(startPosition, endPosition, timeElapsed / _pullTime);
            yield return new WaitForEndOfFrame();
        }

        CaptureBall(ball);
    }

    private void CaptureBall(Transform ball)
    {
        OnCatch.Invoke();
        if (KeepMode)
        {
            
            //do keep mode stuff
        }
        else
        {
            Invoke("ReturnBall", EjectTime);
        }
    }

    private void ReturnBall()
    {
        OnEject.Invoke();

        _pinballBody.isKinematic = false;
        Vector3 force = (transform.forward + transform.up * .5f).normalized * _ejectForce;
        _pinballBody.AddForce(force);
        _pinballBody = null;
    }

    public void TeleportTo(PinballCatcher other)
    {
        if (_pinball != null)
        {
            _pinball.transform.position = other.transform.position;
        }
    }

    public override void Reset()
    {
        Destroy(_pinball);
        _pinballBody = null;
    }
}
