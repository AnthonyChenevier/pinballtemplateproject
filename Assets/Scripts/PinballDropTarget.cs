﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PinballDropTarget : PinballObject, ISequenceElement
{
    public UnityEvent OnTargetHit;

    [SerializeField]
    private float _resetTime = 2;

    [SerializeField]
    private bool _inGroup;

    public bool IsDropped { get; private set; }

    public bool InGroup
    {
        get { return _inGroup; }
        set { _inGroup = value; }
    }


    public PinballSequence Sequence { get; set; }

    [SerializeField]
    private string _elementMessage;
    public string ElementMessage {
        get { return _elementMessage; }
    }

    [SerializeField]
    private string _sequenceElementName;
    public string ElementName {
        get { return _sequenceElementName; }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Pinball"))
        {
            OnTargetHit.Invoke();
            Drop();
            if (Sequence != null)
                Sequence.ProcessElement(this);
        }
    }

    public void Drop()
    {
        IsDropped = true;
        //temporary
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        if (!_inGroup) Invoke("Reset", _resetTime);
    }
    

    public override void Reset()
    {
        IsDropped = false;
        GetComponent<Renderer>().enabled = true;
        GetComponent<Collider>().enabled = true;
       
    }

}
