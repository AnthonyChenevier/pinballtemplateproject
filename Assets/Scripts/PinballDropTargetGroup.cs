﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PinballDropTargetGroup : PinballObject, ISequenceElement
{
    public UnityEvent OnGroupDropped;

    [SerializeField]
    private PinballDropTarget[] _targets;

    [SerializeField]
    private float _resetTime = .5f;

    public PinballSequence Sequence { get; set; }

    [SerializeField]
    private string _elementMessage;
    public string ElementMessage {
        get { return _elementMessage; }
    }

    [SerializeField]
    private string _sequenceElementName;
    private bool _resetting;

    public string ElementName {
        get { return _sequenceElementName; }
    }

    void Start()
    {
        foreach (PinballDropTarget target in _targets)
        {
            target.InGroup = true;
        }
    }

    // Update is called once per frame
	void LateUpdate ()
	{
	    if (!_targets.All(t => t.IsDropped))
	        return;
        if (!_resetting)
        {
            _resetting = true;
            Invoke("Reset", _resetTime);

            OnGroupDropped.Invoke();
        }
	}

    public override void Reset()
    {
        foreach (PinballDropTarget target in _targets)
        {
            target.Reset();
        }
        _resetting = false;
    }
}
