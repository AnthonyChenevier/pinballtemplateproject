﻿using System;
using UnityEngine;

public class PinballFlipper : MonoBehaviour
{
    [Serializable]
    private enum Direction
    {
        Clockwise,
        CounterClockwise
    } 

    [SerializeField]
    private HingeJoint _flipperHinge;

    [SerializeField]
    private KeyCode _flipperKey;

    [SerializeField]
    private Direction _flipDirection;



    //these can change at runtime for testing.
    public float FlipVelocity = 1000;
    public float FlipForce = 750;
    [Range(-90, 0)]
    public float _minAngle = -30;
    [Range(0, 90)]
    public float _maxAngle = 30;
    
    private float _flipVelocity;

    void Start()
    {

        SetLimits();
    }

    private void SetLimits()
    {
        switch (_flipDirection)
        {
            case Direction.Clockwise:
                _flipVelocity = -FlipVelocity;
                break;
            case Direction.CounterClockwise:
                _flipVelocity = FlipVelocity;
                break;
        }

        JointLimits limits = _flipperHinge.limits;
        limits.min = _minAngle;
        limits.max = _maxAngle;
        _flipperHinge.limits = limits;
    }

    void Update()
    {
        if (Input.GetKeyUp(_flipperKey))
        {
            SetLimits();
            Flip(false);
        }

        if (Input.GetKeyDown(_flipperKey))
        {
            SetLimits();
            Flip(true);
        }
    }

    private void Flip(bool flipUp)
    {
        JointMotor hingeMotor = _flipperHinge.motor;
        hingeMotor.force = FlipForce;
        hingeMotor.targetVelocity = !flipUp ? _flipVelocity : -_flipVelocity;
        _flipperHinge.motor = hingeMotor;
    }
}