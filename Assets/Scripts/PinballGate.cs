﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinballGate : MonoBehaviour
{
    [SerializeField]
    private Animator _animation;

    [SerializeField]
    private bool _startOpen;

    public void Start()
    {
        _animation.SetBool("open", _startOpen);
    }


    public void ToggleOpen(bool open)
    {
        _animation.SetBool("open", open);
    }

}
