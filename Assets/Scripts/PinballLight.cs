﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinballLight : PinballObject
{
    [SerializeField]
    private Renderer _emissiveLightRenderer;

    [SerializeField]
    private Light _light;

    private Material _renderMaterial;

    private bool _isOn;

    private float _emissionOriginalHue;
    private float _emissionOriginalSat;

    private float _emissionOriginalValue;

    private Color _emissionColor;

    private float _blinkInterval;

    void Start()
    {
        _renderMaterial = _emissiveLightRenderer.material;
        //get our original color values (emission is controlled by hsv value)
        _emissionColor = _renderMaterial.GetColor("_EmissionColor");
        Color.RGBToHSV(_emissionColor, out _emissionOriginalHue, out _emissionOriginalSat, out _emissionOriginalValue);

        //turn it of to begin
        _isOn = false;
        SetEmissiveValue(0);
        //with no blink
        _blinkInterval = 0;
    }

    private void SetEmissiveValue(float value)
    {
        _emissionColor = Color.HSVToRGB(_emissionOriginalHue, _emissionOriginalSat, value);
        _light.color = _emissionColor;
        _renderMaterial.SetColor("_EmissionColor", _emissionColor);
    }

    public void TurnOn(bool on)
    {
        if ((_isOn && on) || (!_isOn && !on))
            return;
        SetEmissiveValue(on ? _emissionOriginalValue : 0);
        _isOn = on;
    }

    public override void Reset()
    {
        SetEmissiveValue(0);
        _isOn = false;
    }

    //TODO: BLINKING
    //public void SetBlinkInterval(float interval)
    //{

    //}

    //private IEnumerator Blink()
    //{
    //    float timeElapsed = 0;
    //    while (_blinkInterval > 0)
    //    {
    //        if (timeElapsed >= _blinkInterval)
    //        {

    //        }
    //        timeElapsed += Time.deltaTime;
    //    }
    //}
}
