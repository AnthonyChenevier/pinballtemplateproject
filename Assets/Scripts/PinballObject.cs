﻿using UnityEngine;

public abstract class PinballObject: MonoBehaviour
{
    public abstract void Reset();
}