﻿using UnityEngine;

public class PinballPlunger : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _plungerHead;

    [SerializeField]
    private KeyCode _plungerKey;

    [SerializeField]
    private float _maxPullbackForce;

    [SerializeField]
    private AnimationCurve _forceCurve;

    private float _pullbackTime = 0;
    private bool _doPullback;

    void Update()
    {
        if (Input.GetKeyUp(_plungerKey))
        {
            _doPullback = false;
        }
        else if (Input.GetKey(_plungerKey))
        {
            _doPullback = true;
        }
    }

    // FixedUpdate is called once per physics frame (default rate 50fps)
    void FixedUpdate () {
        if (_doPullback)
	    {
            _plungerHead.AddForce(Vector3.back * _forceCurve.Evaluate(_pullbackTime) * _maxPullbackForce, ForceMode.Force);
	        _pullbackTime += Time.deltaTime;
	    }
	    else
	        _pullbackTime = 0;
	}
}
