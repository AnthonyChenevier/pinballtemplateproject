﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PinballSlingshot : MonoBehaviour
{
    [SerializeField]
    private Animation _animation;
    [SerializeField]
    private float _pushForce = 2;

    public UnityEvent OnHit;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Pinball"))
            return;

        OnHit.Invoke();

        if (_animation != null)
            _animation.Play();

        Rigidbody body = other.GetComponent<Rigidbody>();
        body.AddForce(transform.forward * _pushForce, ForceMode.Impulse);
    }
}
