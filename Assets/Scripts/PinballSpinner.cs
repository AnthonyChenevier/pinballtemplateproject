﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class PinballSpinner : PinballObject
{
    [SerializeField]
    private float _forceMultiplier = 5;

    [SerializeField]
    float _dragFactor = 0.99f;

    [SerializeField]
    private float _maxSpinTime = 6f;

    [SerializeField]
    private Animator _spinnerFlap;

    public UnityEvent OnHit;
    public UnityEvent OnSpin;
    private float _speed;
    private float _spinStartTime;
    private int _direction;

    public void BallHit(Vector3 velocity)
    {
        OnHit.Invoke();

        _spinStartTime = Time.time;

        _direction = Vector3.Dot(velocity.normalized, transform.forward) < 0 ? -1 : 1;
        _speed = velocity.magnitude * _forceMultiplier;

        _spinnerFlap.SetFloat("speed", _speed * _direction);//, _spinnerFlap.transform.position + Vector3.down * .2f, ForceMode.Force);
        _spinnerFlap.SetBool("spinning", true);
        Debug.Log(name + " hit");
        StartCoroutine("SlowSpinner");
    }

    private IEnumerator SlowSpinner()
    {
        float timeElapsed = 0;
        while (timeElapsed < _maxSpinTime)
        {
            timeElapsed = Time.time - _spinStartTime;
            //Debug.Log(timeElapsed);
            _speed *= _dragFactor;
            
            _spinnerFlap.SetFloat("speed", _speed * _direction);//, _spinnerFlap.transform.position + Vector3.down * .2f, ForceMode.Force);
            yield return new WaitForEndOfFrame();
        }

        _spinnerFlap.SetBool("spinning", false);
    }

    public override void Reset()
    {
        _spinnerFlap.SetBool("spinning", false);
    }
}
