﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinballSpinnerBallTrigger : MonoBehaviour
{
    [SerializeField]
    private PinballSpinner _spinner;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Pinball"))
            return;

        _spinner.BallHit(other.attachedRigidbody.velocity);
    }
}
