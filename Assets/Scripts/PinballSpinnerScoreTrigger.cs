﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinballSpinnerScoreTrigger : MonoBehaviour
{
    [SerializeField]
    private PinballSpinner _spinner;
    [SerializeField]
    private Collider _spinStriker;

    private void OnTriggerEnter(Collider other)
    {
        if (other == _spinStriker)
        {
            _spinner.OnSpin.Invoke();
        }
    }
}
