﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Events;

public class PinballTable : MonoBehaviour
{
    [Range(0, 10)]
	public float InclinationAngle;

    [SerializeField]
    private GameObject _pinballPrefab;

    [SerializeField]
    private Transform _ballSpawnPoint;

    [SerializeField]
    private PinballHudController _hudController;

    [SerializeField]
    private int _extraBallThreshold;

    [SerializeField]
    private float _freeRetryMaxTime;
    [SerializeField]
    private int _freeRetryMaxTriggers;

    [SerializeField]
    private int _deathBonus = 2000;
    [SerializeField]
    private int _startingBalls = 3;
    private int _ballsRemaining;

    private int _score;
    private float _spawnTime;
    private int _triggersHit;
    private bool _retryUsed;

    public int CurrentMultiplier = 1;
    private int _lastExtraBallScore;
    private PinballObject[] _resetables;

    public UnityEvent RetryEnabled;
    public UnityEvent RetryDisabled;
    private bool _retryDisabled;

    // Use this for initialization
    void Start ()
    {
        //just rotate gravity here instead of having to tilt the board in-editor
        Vector3 angleGravity = Physics.gravity;
        angleGravity = Quaternion.AngleAxis(InclinationAngle, Vector3.right) * angleGravity;
        Physics.gravity = angleGravity;

        _resetables = FindObjectsOfType<PinballObject>();

        StartNewGame();
    }
    

    public void StartNewGame()
    {
        _score = 0;
        _ballsRemaining = _startingBalls;
        _triggersHit = 0;

        foreach (PinballObject resetable in _resetables)
        {
            resetable.Reset();
        }

        _hudController.ResetUI(_startingBalls);
        RetryEnabled.Invoke();
        SpawnNewBall();
    }

    public void Update()
    {
        if (_retryDisabled || RetryAllowed())
            return;

        _retryDisabled = true;
        RetryDisabled.Invoke();
    }


    public void Bump()
    {
        //todo
    }

    public void AddPoints(int points)
    {
        _score += points * CurrentMultiplier;
        _hudController.SetScore(_score);

        if (_score > _lastExtraBallScore && _score % _extraBallThreshold == 0)
        {
            AddExtraBall();
            _lastExtraBallScore = _score;
        }
    }

    private void AddExtraBall()
    {
        _ballsRemaining++;
        _hudController.SetLives(_ballsRemaining);
        _hudController.SetStatusMessage("*Free Ball*");
    }

    public void DestroyBall(Pinball ball)
    {
        ball.DestroySelf();
    }

    public void GutterBall(Pinball ball)
    {
        ball.DestroySelf();
        _triggersHit = 0;

        //first check if the player was unlucky and give them a free retry
        if (RetryAllowed())
        {
            _hudController.SetStatusMessage("**Free Retry**");
            SpawnNewBall();
            _retryUsed = true; //you only get one retry per ball
        }
        else if (_ballsRemaining - 1 == 0) //no balls left
        {
            AddPoints(_deathBonus);
            EndGame();
        }
        else //just lose a life and reset for the next ball
        {
            AddPoints(_deathBonus);
            _ballsRemaining--;
            _hudController.SetLives(_ballsRemaining);
            _retryUsed = false;
            _retryDisabled = false;
            RetryEnabled.Invoke();
            _hudController.SetStatusMessage("**Try Again**");
            SpawnNewBall();
        }
    }

    public void SpawnNewBall()
    {
        GameObject pinballInstance = Instantiate(_pinballPrefab);
        pinballInstance.transform.position = _ballSpawnPoint.position;
        _spawnTime = Time.time;
    }

    private bool RetryAllowed()
    {
        return !_retryUsed && (Time.time - _spawnTime) <= _freeRetryMaxTime && _triggersHit <= _freeRetryMaxTriggers;
    }

    private void EndGame()
    {
        _hudController.SetStatusMessage("GAME OVER");

        //todo: add multiplier adding sequence?

        _hudController.ShowGameOver();
        _hudController.ShowHighScore(_score);
    }

    public void CountTrigger() { _triggersHit++; }
}