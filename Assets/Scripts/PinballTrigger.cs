﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PinballTrigger : MonoBehaviour, ISequenceElement
{
    [Serializable]
    public class PinballEvent : UnityEvent<Pinball> { }

    public PinballEvent OnTriggerEntered;
    public PinballEvent OnTriggerExited;


    public PinballSequence Sequence { get; set; }

    [SerializeField]
    private string _elementMessage;
    public string ElementMessage {
        get { return _elementMessage; }
    }

    [SerializeField]
    private string _sequenceElementName;
    public string ElementName {
        get { return _sequenceElementName; }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pinball"))
        {
            OnTriggerEntered.Invoke(other.GetComponent<Pinball>());
            if (Sequence != null)
                Sequence.ProcessElement(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pinball"))
        {
            OnTriggerExited.Invoke(other.GetComponent<Pinball>());
        }
    }
}
