﻿public interface ISequenceElement
{
    PinballSequence Sequence { get; set; }
    string ElementMessage { get; }
    string ElementName { get; }
}
