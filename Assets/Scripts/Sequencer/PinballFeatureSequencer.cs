﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinballFeatureSequencer : MonoBehaviour
{
    [SerializeField]
    private List<PinballSequence> _sequenceList;

	// Use this for initialization
	void Start ()
    {
	    foreach (PinballSequence sequence in _sequenceList)
	    {
	        sequence.Init(this);
	    }
	}
}
