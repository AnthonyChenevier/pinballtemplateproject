﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class PinballSequence
{

    [Serializable]
    public class ElementEvent : UnityEvent<string> { }

    private PinballFeatureSequencer _sequencer;

    [SerializeField]
    private string _name;

    [SerializeField]
    private bool _isOrdered;

    [SerializeField]
    private bool _isActive;
    public bool IsActive { get { return _isActive; } set { _isActive = value; } }

    [SerializeField]
    private List<MonoBehaviour> _elementObjects;
    
    public List<ISequenceElement> Elements;


    public UnityEvent OnBegin;
    public ElementEvent OnElement;
    public UnityEvent OnComplete;
    public UnityEvent OnFail;

    //ordered list index
    private int _currentIndex;

    private int _elementsCompleted;


    public void Init(PinballFeatureSequencer sequencer)
    {
        Elements = new List<ISequenceElement>();
        foreach (MonoBehaviour o in _elementObjects)
        {
            ISequenceElement element = o as ISequenceElement;
            if (element == null)
            {
                Debug.LogError("Sequence (" + _name + ") INITIALIZATION ERROR: Object '" + o.name + "' is not a sequence element");
                return;
            }
            
            Elements.Add(element);
        }
        _sequencer = sequencer;
        foreach (ISequenceElement element in Elements)
        {
            element.Sequence = this;
        }

        Reset();
    }


    public void ProcessElement(ISequenceElement element)
    {
        if (!IsActive) return;
        //wrong sequence/element combo. Shouldn't happen.
        if (!Elements.Contains(element))
        {
            Debug.LogError("SEQUENCE PROCESSING ERROR: Sequence '" + _name + "' does not contain element '"+ element.ElementName +"'");
            return;
        }

        int elementIndex = Elements.IndexOf(element);
        //send the begin message if we are at the start of this sequence
        if (_elementsCompleted == 0)
        {
            if (!_isOrdered)
            {
                OnBegin.Invoke();
            }
            //if we are ordered only continue if this is the first element
            else if (_isOrdered && elementIndex == 0)
            {
                OnBegin.Invoke();
            }
            else if (_isOrdered && elementIndex > 0)
            {
                return;
            }
        }

        //if sequence is unordered then just increment
        if (!_isOrdered)
        {
            Increment();
        }
        //otherwise check that the element is the correct 
        //one then increment or fail as required
        else
        {
            if (elementIndex == _currentIndex + 1)
            {
                Increment();
            }
            else
            {
                Fail();
            }
        }
    }

    private void Reset()
    {
        _currentIndex = -1;
        _elementsCompleted = 0;
        //casts each sequence element to a pinball object then resets
        foreach (ISequenceElement element in Elements)
        {
            PinballObject obj = element as PinballObject;
            if (obj != null)
            {
                obj.Reset();
            }
        }
    }

    private void Increment()
    {
        _currentIndex++;
        _elementsCompleted++;
        if (_elementsCompleted == Elements.Count)
        {
            Complete();
        }
        //supress the first element message for the begin message
        else if (_elementsCompleted > 1)
        {
            OnElement.Invoke(Elements[_currentIndex].ElementMessage);
        }
    }

    private void Fail()
    {
        Reset();
        OnFail.Invoke();
    }


    private void Complete()
    {
        Reset();
        OnComplete.Invoke();
    }
}
