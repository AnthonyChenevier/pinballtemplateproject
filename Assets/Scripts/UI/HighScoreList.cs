﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreList : UIPanel
{
    [SerializeField]
    private List<ScoreEntry> _scores;

    [SerializeField]
    private int _maxEntries = 10;

    [SerializeField]
    private LayoutGroup _scoreEntryList;

    [SerializeField]
    private ScoreEntry _scoreEntryPrefab;

    public int HighestScore {
        get
        {
            // 0 index should always be highest as entries are sorted
            return _scores.Count == 0 ? 0 : _scores[0].Score;
        }
    }

    public int LowestScore {
        get
        {
            // last index should always be lowest as entries are sorted
            return _scores.Count == 0 ? 0 : _scores[_scores.Count - 1].Score;
        }
    }


    public void AddHighScore(string initials, int score)
    {
        //remove all entries from the ui list
        _scoreEntryList.transform.DetachChildren();

        ScoreEntry entry = Instantiate(_scoreEntryPrefab);
        entry.Initials = initials;
        entry.Score = score;

        _scores.Add(entry);

        //sort by highest score
        _scores.Sort((a, b) => b.Score - a.Score);

        //remove entries at the end if necessary
        while (_scores.Count > _maxEntries)
        {
            int lastIndex = _scores.Count - 1;
            ScoreEntry last = _scores[lastIndex];

            _scores.RemoveAt(lastIndex);

            Destroy(last.gameObject);
        }

        // add each entry back in the new order
        foreach (ScoreEntry scoreEntry in _scores)
        {
            scoreEntry.transform.SetParent(_scoreEntryList.transform, false);
            scoreEntry.transform.localScale = Vector3.one;
        }
    }
}
