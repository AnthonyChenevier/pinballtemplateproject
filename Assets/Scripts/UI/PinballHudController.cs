﻿using UnityEngine;
using UnityEngine.UI;

public class PinballHudController : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Text _livesText;

    [SerializeField]
    private Text _statusText;

    [SerializeField]
    private float _defaultMessageTime = 3f;
    
    [SerializeField]
    private UIPanel _mainGamePanel;

    [SerializeField]
    private UIPanel _gameOverPanel;

    [SerializeField]
    private HighScoreList _highScorePanel;

    [SerializeField]
    private ScoreEntryUI _scoreEntryPanel;


    private void Start()
    {
        //show the main panel
        _mainGamePanel.Show(true);

        //hide everything else
        _gameOverPanel.Show(false);
        _highScorePanel.Show(false);
        _scoreEntryPanel.Open(0, false);
    }

    public void ResetUI(int defaultBalls)
    {
        SetLives(defaultBalls);
        SetScore(0);
        ClearStatusMessage();

        ShowGameHud();
        ShowHighScore(0, false);
        ShowGameOver(false);
    }

    
    public void SetScore(int score)
    {
        _scoreText.text = score.ToString("000000000000");
    }

    public void SetLives(int lives)
    {
        _livesText.text = lives.ToString();
    }

    public void SetStatusMessage(string msg)
    {
        SetStatusMessage(msg, _defaultMessageTime);
    }

    public void SetStatusMessage(string msg, float duration)
    {
        _statusText.text = msg;
        //clear after time elapsed
        Invoke("ClearStatusMessage", duration);
    }

    public void ClearStatusMessage()
    {
        _statusText.text = "";
    }

    public void ShowGameOver(bool show = true)
    {
        _gameOverPanel.Show(show);
    }

    public void ShowGameHud(bool show = true)
    {
        _mainGamePanel.Show(show);
    }

    public void ShowHighScore(int score, bool show = true)
    {
        _highScorePanel.Show(show);

        if (!show)
            return;

        // show high score name entry panel if we made it on the 
        // list (higher than the current lowest score on the list)
        if (score > _highScorePanel.LowestScore)
        {
            _scoreEntryPanel.Open(score);
        }
    }
}
