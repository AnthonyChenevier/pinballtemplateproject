﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreEntry: MonoBehaviour
{
    private string _initials;
    [SerializeField]
    private int _score;

    [SerializeField]
    private Text _initialsText;

    [SerializeField]
    private Text _scoreText;


    public string Initials
    {
        get { return _initials; }
        set
        {
            _initials = value;
            _initialsText.text = value;
        }
    }

    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            _scoreText.text = value.ToString("000000000000");
        }
    }
}