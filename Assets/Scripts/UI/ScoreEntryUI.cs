﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreEntryUI : UIPanel {
    [SerializeField]
    private Button _submitButton;

    [SerializeField]
    private InputField _input;

    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private HighScoreList _scoreList;

    private int _score;

    private bool _canSubmit;


    //shows the high score entry screen
    public void Open(int score, bool show = true)
    {
        _scoreText.text = score.ToString("000000000000");
        _score = score;

        Show(show);

        //use invoke to wait a bit so the focus is possible
        _input.Select();
        _input.ActivateInputField();
    }

    // tracks input field. Converts input to uppercase and disables submission unless entry is 3 chars long
    public void TrackInput ()
    {
        _canSubmit = _input.text.Length == 3;
        _submitButton.interactable = _canSubmit;

        //make sure it's only upper case
        _input.text = _input.text.ToUpper();
    }

    // adds a new high score entry to the list and closes the screen
    public void Submit()
    {
        if (!_canSubmit)
            return;

        _scoreList.AddHighScore(_input.text, _score);

        Show(false);

        _input.text = "";
    }
}
