﻿using UnityEngine;
using UnityEngine.UI;

public class TextEntrySubmitButton : MonoBehaviour {
    private Button _button;

    // Use this for initialization
	void Start () { _button = GetComponent<Button>(); }
	
	// Update is called once per frame
	public void EnableIfNotEmpty (InputField input)
	{
	    if (input.textComponent.text != "")
	        _button.interactable = true;
	}
}
