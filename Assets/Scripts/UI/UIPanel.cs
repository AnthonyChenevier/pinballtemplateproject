﻿using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UIPanel : MonoBehaviour
{

    private CanvasGroup _canvasGroup;


    void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Show(bool show)
    {
        _canvasGroup.alpha = show ? 1 : 0;
        _canvasGroup.interactable = show;
        _canvasGroup.blocksRaycasts = show;
    }
}
