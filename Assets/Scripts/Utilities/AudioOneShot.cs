﻿using UnityEngine;
using System.Collections;

public class AudioOneShot : MonoBehaviour {
    AudioSource _source;
    public AudioClip Clip;
	// Use this for initialization
    private void Start () {
        _source = GetComponent<AudioSource>() ?? gameObject.AddComponent<AudioSource>();
	}
	
	// Update is called once per frame
	public void Play () {
        _source.PlayOneShot(Clip);
	}
}
