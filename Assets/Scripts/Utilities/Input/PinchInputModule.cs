﻿using UnityEngine;
using UnityEngine.EventSystems;

/* note, Unity chooses to have "one interface for each action"
however here we are dealing with a consistent paradigm ("pinching")
which has three parts; I feel it's better to have one interface
forcing the consumer to have the three calls (no problem if empty) */

public class PinchInputModule : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
    // of course that would be a List,
    // just one shown for simplicity in this example code

    private int currentFirstFinger = -1;
    private int currentSecondFinger = -1;
    private int kountFingersDown = 0;
    private bool pinching = false;

    private Vector2 positionFirst = Vector2.zero;
    private Vector2 positionSecond = Vector2.zero;
    private float previousDistance = 0f;
    private float delta = 0f;
    private IPinchHandler _myHandler;

    void Awake() { _myHandler = GetComponent<IPinchHandler>(); }


    public void OnPointerDown(PointerEventData data)
    {
        kountFingersDown = kountFingersDown + 1;

        if (currentFirstFinger == -1 && kountFingersDown == 1)
        {
            // first finger must be a pure first finger and that's that

            currentFirstFinger = data.pointerId;
            positionFirst = data.position;

            return;
        }

        if (currentFirstFinger == -1 || currentSecondFinger != -1 || kountFingersDown != 2) return;
        // second finger must be a pure second finger and that's that

        currentSecondFinger = data.pointerId;
        positionSecond = data.position;

        FigureDelta();

        pinching = true;
        if (_myHandler != null) _myHandler.OnStartPinch();
    }

    public void OnPointerUp(PointerEventData data)
    {
        kountFingersDown = kountFingersDown - 1;

        if (currentFirstFinger == data.pointerId)
        {
            currentFirstFinger = -1;

            if (pinching)
            {
                pinching = false;
                if (_myHandler != null) _myHandler.OnEndPinch();
            }
        }

        if (currentSecondFinger != data.pointerId) return;

        currentSecondFinger = -1;

        if (!pinching) return;

        pinching = false;
        if (_myHandler != null) _myHandler.OnEndPinch();
    }

    public void OnDrag(PointerEventData data)
    {
        if (currentFirstFinger == data.pointerId)
        {
            positionFirst = data.position;
            FigureDelta();
        }

        if (currentSecondFinger == data.pointerId)
        {
            positionSecond = data.position;
            FigureDelta();
        }

        if (!pinching) return;

        if (data.pointerId != currentFirstFinger && data.pointerId != currentSecondFinger) return;

        if (kountFingersDown == 2)
        {
            if (_myHandler != null) _myHandler.OnPinch(delta);
        }
    }

    private void FigureDelta()
    {
        float newDistance = Vector2.Distance(positionFirst, positionSecond);
        delta = newDistance - previousDistance;
        previousDistance = newDistance;
    }

}